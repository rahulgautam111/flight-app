package com.flightService.service;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;

import com.flightService.dao.BookingRepository;
import com.flightService.dto.BookingDto;
import com.flightService.entity.Booking;

public class BookingServiceImpl implements BookingService {

	@Autowired
	private final ModelMapper modelMapper;
	private final BookingRepository bookingRepository;

	public BookingServiceImpl(ModelMapper modelMapper, BookingRepository bookingRepository) {
		this.modelMapper = modelMapper;
		this.bookingRepository = bookingRepository;
	}

	@Override
	public BookingDto bookFlight(BookingDto bookingDto) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Booking booking = modelMapper.map(bookingDto, Booking.class);
		return modelMapper.map(bookingRepository.save(booking), BookingDto.class);
	}

}
