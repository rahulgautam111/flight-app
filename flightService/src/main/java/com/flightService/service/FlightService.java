package com.flightService.service;

import java.util.List;

import com.flightService.dto.FlightDto;

public interface FlightService {

	public FlightDto addFlight(FlightDto flightDto);
	
	public FlightDto blockFlight(String flightId);

	public List<FlightDto> searchFlights(String source, String destination);
}
