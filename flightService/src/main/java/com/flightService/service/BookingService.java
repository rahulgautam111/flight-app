package com.flightService.service;

import java.util.List;

import com.flightService.dto.BookingDto;

public interface BookingService {

	BookingDto bookFlight(BookingDto bookingDto);

}
