package com.flightService.service;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flightService.dao.FlightRepository;
import com.flightService.dto.BookingDto;
import com.flightService.dto.FlightDto;
import com.flightService.entity.Booking;
import com.flightService.entity.Flight;

@Service
public class FlightServiceImpl implements FlightService {

	private final ModelMapper modelMapper;
	private final FlightRepository flightRepository;

	@Autowired
	public FlightServiceImpl(ModelMapper modelMapper, FlightRepository flightRepository) {
		this.modelMapper = modelMapper;
		this.flightRepository = flightRepository;
	}

	@Override
	public FlightDto addFlight(FlightDto flightDto) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		Flight flight = modelMapper.map(flightDto, Flight.class);

		Flight res = flightRepository.save(flight);

		// TODO Auto-generated method stub
		return modelMapper.map(res, FlightDto.class);
	}

	@Override
	public FlightDto blockFlight(String flightId) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Flight flight = flightRepository.findByFlightId(flightId);

		flight.setStatus("blocked");

		Flight res = flightRepository.save(flight);

		return modelMapper.map(res, FlightDto.class);
	}

	@Override
	public List<FlightDto> searchFlights(String source, String destination) {
		// TODO Auto-generated method stub
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		List<FlightDto> flights = new ArrayList<>();
		List<Flight> res = flightRepository.findBySourceAndDestination(source,destination);
		
		for(Flight flight : res) {
			flights.add(modelMapper.map(flight, FlightDto.class));
		}
		
		return flights;
		
		
	}

}
