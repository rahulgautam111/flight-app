package com.flightService.entity;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "bookedflights")
public class Booking {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String bookingId;

	private String flightNumber;

	private String date;

	private String source;

	private String destination;

	private String name;

	private String email;

	private Integer totalSeats;

	private String mealType;

	@ElementCollection
	private List<Passengers> passenger = new ArrayList<>();

}
