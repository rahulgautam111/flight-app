package com.flightService.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "flights")

public class Flight {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "flight_id", nullable = false, unique = true)
	private String flightId;

	private String airLine;

	private String source;

	private String destination;

	private String startDate;

	private String endDate;

	private String instrumentUsed;

	private Integer businessClassSeats;

	private Integer noBusinessClassSeats;

	private long ticketCost;

	private Integer totalRows;

	private String meal;

	private String status;
}
