package com.flightService.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class BookingDto {
	
	private String BookingId;

	private String flightNumber;

	private String date;

	private String source;

	private String destination;

	private String name;

	private String email;

	private int totalSeats;

	private String mealType;

}
