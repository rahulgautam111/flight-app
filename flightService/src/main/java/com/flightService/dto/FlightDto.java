package com.flightService.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FlightDto {
	
	private String flightId;

	private String airLine;

	private String source;

	private String destination;

	private String startDate;

	private String endDate;

	private String instrumentUsed;

	private Integer businessClassSeats;

	private Integer noBusinessClassSeats;

	private long ticketCost;

	private Integer totalRows;

	private String meal;

	private String status;

}
