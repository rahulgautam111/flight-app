package com.flightService.controller;

import java.util.List;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flightService.dto.BookingDto;
import com.flightService.service.BookingService;

@RestController
@RequestMapping(name = "/api/v1.0/flight")
@CrossOrigin(origins = "*")
public class BookingController {

	@Autowired
	private BookingService bookingService;

	private ModelMapper modelMapper;

	public BookingController(BookingService bookingService, ModelMapper modelMapper) {
		this.bookingService = bookingService;
		this.modelMapper = modelMapper;
	}

	@PostMapping("/booking")
	public ResponseEntity<BookingDto> bookFlight(@RequestBody BookingDto bookingDto) {

		String flightId[] = UUID.randomUUID().toString().split("-");
		bookingDto.setBookingId(flightId[0]);
		BookingDto res = bookingService.bookFlight(bookingDto);
		return ResponseEntity.status(HttpStatus.OK).body(res);

	}

	@PutMapping("/airlines/")
	public ResponseEntity<BookingResponseModel> updateFlight(@RequestBody BookingDto bookingDto) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		BookingDto res  = bookingService.updateFlight(bookingDto);
		
	return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(res, BookingResponseModel.class));

//	}
}
