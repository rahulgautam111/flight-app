package com.flightService.controller;

import java.util.List;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.flightService.dto.BookingDto;
import com.flightService.dto.FlightDto;
import com.flightService.entity.Flight;
import com.flightService.service.FlightService;

@RestController
public class FlightController {
	@Autowired
	private FlightService flightService;

	private ModelMapper modelMapper;

	public FlightController(FlightService flightService, ModelMapper modelMapper) {
		this.flightService = flightService;
		this.modelMapper = modelMapper;
	}
	
	@PostMapping("/airline/inventory/add")
	public ResponseEntity<FlightDto> addNewFlight(@RequestBody FlightDto flightDto) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		String[] uuid = UUID.randomUUID().toString().split("-");

		flightDto.setFlightId("Flight -" + uuid[0]);

		FlightDto res = flightService.addFlight(flightDto);

		return ResponseEntity.status(HttpStatus.CREATED).body(res);

	}
	
	@PutMapping("/airline/inventory/add/{flightId}")
	public ResponseEntity<FlightDto> blockFlight(@PathVariable String flightId) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		FlightDto res = flightService.blockFlight(flightId);

		return ResponseEntity.status(HttpStatus.CREATED).body(res);

	}
	

	@GetMapping("/airlines/search/")
	public ResponseEntity<List<FlightDto>> searchFlights(@RequestHeader String source,
			@RequestHeader String destination) {

		List<FlightDto> res = flightService.searchFlights(source, destination);

		return ResponseEntity.status(HttpStatus.OK).body(res);

	}
}
